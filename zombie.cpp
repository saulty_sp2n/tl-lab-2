#include <iostream>     // Used for cin and cout
#include <iomanip>      // Used for formatting cout statements
#include <string>       // Used for string data types
#include <cstdlib>      // Used for rand()
#include <ctime>        // time used to seed random number generator
using namespace std;

// adding a comment per the instructions

void DisplayMenu(int day, int food, int health, int maxHealth, string name, string location);

int main()
{
    /******************************************************************* INITIALIZE VARIABLES */
    bool done = false;                      // Flag for if the game is over
    bool successfulAction = false;          // Flag for if the user entered valid input
    int food = 5;                           // Player stat - how much food they have
    int maxHealth = 20;                     // Player stat - their maximum amount of health
    int health = maxHealth;                 // Player stat - their current health amount
    string name = "ME";                     // Player stat - what their name is
    string location = "Overland Park";      // Player stat - what location they're in
    int day = 1;                            // Player stat - how many days it has been

    string dump;                            // A variable to dump input into
    int choice;                             // A variable to dump input into
    int mod;                                // A variable used for calculations later

    srand(time(NULL));                  // Seeding the random number generator

    /******************************************************************* GAME START */
    cout << "Z O M B I E  -  A P O C A L Y P S E" << endl;
    cout << "Enter your name: ";
    getline(cin, name);

    cout << endl;

    /******************************************************************* GAME LOOP */
    while (!done)
    {
        /** Start of the round **/
        successfulAction = false;
        DisplayMenu(day, food, health, maxHealth, name, location);
        cout << "CHOICE: ";
        cin >> choice;

        /** Student implements features here **/
        
        cout << "*The day passes (+1 day)." << endl;
        day++;

        if (food > 0)
        {
            cout << "*You eat a meal (-1 food)." << endl;
            food--;
            health++;
        }
        else
        {
            mod = rand() % 4 + 1;
            cout << "*You are starving! (-" << mod << " health)." << endl;
            health = health - mod;
        }

        if (health > maxHealth)
        {
            health = maxHealth;
        }

        if (food <= 0)
        {
            food = 0;
        }

        if (health <= 0)
        {
            cout << "*You have died." << endl;
            done = true;
        }
        else if (day >= 20)
        {
            cout << "In the morning, a group of scavengers find you. They have a fortification nearby and are rebuilding a society. You agree to live in their town. " << endl;
            done = true;
        }

        switch (choice)
        {
        case 1: //scavenging
        {
            cout << "*You scavenge here." << endl;
            successfulAction = true;

            int randomChance = rand() % 5;

            switch (randomChance)
            {
            case 0: // find food
                mod = rand() % 4 + 2;
                cout << "*You find a stash of food. (+" << mod << " food)" << endl;
                food += mod;
                break;

            case 1: // surprised by zombie
                mod = rand() % 7 + 2;
                cout << "*A zombie suprises you! You get hurt in the encounter. (-" << mod << " health)" << endl;
                health -= mod;
                break;

            case 2: // find medical supplies
                mod = rand() % 4 + 2;
                cout << "*You find some medical supplies. (+" << mod << " health)" << endl;
                health += mod;
                break;

            case 3: // ambushed 
                mod = rand() % 5 + 2;
                cout << "*Another scavenger ambushes you! *They take some supplies from you. (-" << mod << " food)" << endl;
                food -= mod;
                break;

            case 4: // nothing
                cout << "You don't find anything." << endl;
                break;

            }
        }
            break;
        
        case 2: // movement
        {

            cout << "Walk to where?" << endl;
            cout << "1. Overland Park" << endl << "2. Raytown" << endl
                << "3. Kansas City" << endl << "4. Gladstone" << endl;
            cin >> choice;

            switch (choice)
            {
            case 1:
                if (location == "Overland Park")
                {
                    cout << "You're already there!" << endl;
                }
                else
                {
                    location = "Overland Park";
                    successfulAction = true;
                }
                break;

            case 2:
                if (location == "Raytown")
                {
                    cout << "You're already there!" << endl;
                }
                else
                {
                    location = "Raytown";
                    successfulAction = true;
                }
                break;

            case 3:
                if (location == "Kansas City")
                {
                    cout << "You're already there!" << endl;
                }
                else
                {
                    location = "Kansas City";
                    successfulAction = true;
                }
                break;

            case 4:

                if (location == "Gladstone")
                {
                    cout << "You're already there!" << endl;
                }
                else
                {
                    location = "Gladstone";
                    successfulAction = true;
                }
                break;

            default:

                cout << "Invalid Selection!" << endl;
                break;

            }

            if (successfulAction)
            {
                cout << "You travel to " << location << "." << endl;
                int randomChance = rand() % 5;

                switch (randomChance)
                {
                case 0: // zombie fight
                    cout << "*A zombie attacks!" << endl << endl << "You fight it off." << endl;
                    break;

                case 1: // zombie fight 2
                    mod = rand() % 5 + 2;
                    cout << "*A zombie attacks!" << endl << endl << "It bites you as you fight it! -" << mod << " health)" << endl;
                    health -= mod;
                    break;

                case 2: // trade
                    mod = rand() % 3 + 2;
                    cout << "*You find another scavenger and trade goods. (+" << mod << " food)" << endl;
                    food += mod; 
                    break;

                case 3: // safe house
                    mod = rand() % 4 + 2;
                    cout << "*You find a safe building to rest in temporarily (+" << mod << " health)" << endl;
                    health += mod;
                    break;

                case 4: // nothing
                    cout << "The journey is uneventful" << endl;
                    break;
                }
            }
            else
            {
                continue;
            }


            break;
        }

        

        }






        /** End of the round - nothing to update **/
        cout << endl << "Press ENTER to continue...";
        cin.ignore();
        getline(cin, dump);

        cout << "----------------------------------------" << endl;
        cout << "----------------------------------------" << endl;
    }

    /******************************************************************* GAME OVER */
    cout << endl << endl << "You survived the apocalypse on your own for " << day << " days." << endl;

    return 0;
}

/* You don't need to modify this, it just displays the user's information at the start of each round. */
void DisplayMenu(int day, int food, int health, int maxHealth, string name, string location)
{
    cout << "----------------------------------------" << endl;
    cout << left << setw(3) << "-- " << setw(35) << name << "--" << endl;
    cout << left
        << setw(6) << "-- Day " << setw(4) << day
        << setw(6) << "Food: " << setw(4) << food
        << setw(8) << "Health: " << setw(2) << health << setw(1) << "/" << setw(2) << maxHealth
        << right << setw(6) << "--" << endl;
    cout << left
        << setw(10) << "-- Location: " << setw(20) << location
        << right << setw(7) << "--" << endl;
    cout << "----------------------------------------" << endl;
    cout << "-- 1. Scavenge here                   --" << endl;
    cout << "-- 2. Travel elsewhere                 --" << endl;
    cout << "----------------------------------------" << endl;
    for (int i = 0; i < 15; i++)
    {
        cout << endl;
    }
}
